'use strict';

/* Services */

// Simple value service.
var ghosterServices = angular.module('ghoster.services', ['ngResource']).value('version', '0.1');

ghosterServices.factory('Process', [
    function () {
        return {};
    }
]);

ghosterServices.factory('Artist', ['$resource',
    function ($resource) {
        return $resource('/api/v1/artists/:id', {id: '@id'});
    }
]);

ghosterServices.factory('Product', ['$resource',
    function ($resource) {
        return $resource('/api/v1/products/:id', {id: '@id'});
    }
]);

ghosterServices.factory('Customer', ['$resource',
    function ($resource) {
        return $resource('/api/v1/customers/:id', {id: '@id'});
    }
]);

ghosterServices.factory('URLShortener', ['$resource',
    function ($resource) {
        var apiKey = 'AIzaSyBonGOUj_pxYkACRz5s1yn9OZ9gikucxEY';
        return $resource('https://www.googleapis.com/urlshortener/v1/url', {});
    }
]);

ghosterServices.factory('MaxMind', ['$q',
    function ($q) {
        var defer = $q.defer();

        var onSuccess = function(location){
            defer.resolve(location);
        };

        var onError = function(error){
            defer.reject("Error:\n\n" + JSON.stringify(error, undefined, 4));
        };

        var MaxMind = {
            query: function() {
                geoip2.city(onSuccess, onError);

                return defer.promise;
            }
        };

        return MaxMind;
    }
]);
