'use strict';

/* Controllers */

var ghosterControllers = angular.module('ghoster.controllers', []);

// Here we set global functions available to all the controllers and scopes.
// Theses functions are used throughout the app and we don't want to clutter
// the controllers, so we store them in a global place.
ghosterControllers.run(['$rootScope', '$window', 'Process', function($rootScope, $window, Process) {
    $rootScope.sections = [
        {name:gettext('Contribution'), active:false, show:true, index:1},
        {name:gettext('Format'), active:false, show:false, index:2},
        {name:gettext('Confirmation'), active:false, show:true, index:3},
        {name:gettext('End'), active:false, show:true, index:4},
    ];

    $rootScope.activateSection = function(index) {
        angular.forEach($rootScope.sections, function(section, i) {
            if(i === index) {
                section.active = true;
            } else if(i > index) {
                section.active = false;
            }
        });
    };

    $rootScope.disableSection = function(index) {
        angular.forEach($rootScope.sections, function(section, i) {
            if(i === index) {
                section.show = false;
            } else if(i > index) {
                section.index = i;
            }
        });
    };

    $rootScope.enableSection = function(index) {
        angular.forEach($rootScope.sections, function(section, i) {
            if(i === index) {
                section.show = true;
            } else if(i > index) {
                section.index = i+1;
            }
        });
    };

    $rootScope.openFAQ = function() {
        $rootScope.openWindow('faq' + $rootScope.type, 'Frequently Asked Questions', 590, 590);
    };

    $rootScope.openWindow = function(page, title, width, height) {
        var url = '/' + $rootScope.artist.slug + '/' + page + '/';

        var wLeft = $window.screenLeft ? $window.screenLeft : $window.screenX;
        var wTop = $window.screenTop ? $window.screenTop : $window.screenY;
        var left = wLeft + ($window.innerWidth / 2) - (width / 2);
        var top = wTop + ($window.innerHeight / 2) - (height / 2);
        $window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
    };
}]);

ghosterControllers.controller('PayCtrl', ['$rootScope', '$scope', '$window', '$timeout', 'Artist', 'Process', '$location', 'URLShortener', 'MaxMind',
    function($rootScope, $scope, $window, $timeout, Artist, Process, $location, URLShortener, MaxMind) {
        $rootScope.activateSection(0);
        $rootScope.enableSection(1);

        // Init selectedProduct to prevent case where view gets loaded before the data.
        $scope.selectedProduct = {max_price:0, min_price:0, step_price:0, default_price:0};

        $scope.showTxtInput = function() {
            return $scope.selectedProduct.can_be_free || parseInt($scope.selectedPrice) < parseInt($scope.selectedProduct.max_price);
        };

        $scope.loadData = function() {
            MaxMind.query()
                .then(function(data) {
                    Process.maxmind = data;
                })
                .catch(function(error) {
                    console.log(error);
                });

            $scope.artist = Artist.get({id: $scope.artist_id}, function(response) {
                Process.artist = $scope.artist;
                $rootScope.artist = $scope.artist;

                // Loop through product to find right one
                angular.forEach($scope.artist.products, function(product) {
                    if(product.id === $scope.product_id) {
                        $scope.selectedProduct = product;
                        Process.selectedProduct = $scope.selectedProduct;
                    }
                });

                $scope.init();
            });
        };

        $scope.init = function() {
            $scope.selectedPrice = Process.selectedPrice ? Process.selectedPrice : Process.selectedProduct.default_price;
            Process.type = 'pwyw';
            $rootScope.type = 'pwyw';
            $rootScope.url = window.location.pathname;
            Process.selectedPrice = $scope.selectedPrice;
            Process.currentLanguage = $scope.language;
            Process.shareUrl = $location.absUrl();
            Process.shareUrl = Process.shareUrl.substr(0, Process.shareUrl.indexOf('#'));

            // Ugly hack to prevent the slider init to none
            $timeout(function() { $('input[type="range"]').val($scope.selectedPrice); }, 1);
        };

        $scope.$watch('selectedPrice', function(newValue, oldValue) {
            if(newValue !== oldValue) {
                Process.selectedPrice = $scope.selectedPrice;
            }
        });

        $scope.$watch('selectedPriceTxt', function(newValue, oldValue) {
            if(newValue !== oldValue) {
                Process.selectedPrice = $scope.selectedPriceTxt;
            }
        });

        if($scope.artist_id && $scope.product_id) {
            $scope.loadData();
        } else if(!$scope.product_id) {
            $window.location.href = "#/subscribe";
        }
    }
]);

ghosterControllers.controller('SubscribeCtrl', ['$rootScope', '$scope', '$window', '$filter', '$sce', 'Artist', 'Process', '$location', 'MaxMind',
    function($rootScope, $scope, $window, $filter, $sce, Artist, Process, $location, MaxMind) {
        $rootScope.activateSection(0);
        $rootScope.disableSection(1);

        $scope.loadData = function() {
            MaxMind.query()
                .then(function(data) {
                    Process.maxmind = data;
                })
                .catch(function(error) {
                    console.log(error);
                });

            $scope.artist = Artist.get({id: $scope.artist_id}, function(response) {
                Process.artist = $scope.artist;
                $rootScope.artist = $scope.artist;

                // Loop through product to find right one
                angular.forEach($scope.artist.products, function(product) {
                    if(product.id === $scope.product_id) {
                        $scope.selectedProduct = product;
                        Process.selectedProduct = $scope.selectedProduct;
                    }
                });

                $scope.init();
            });
        };

        $scope.init = function() {
            Process.type = 'subscribe';
            $rootScope.type = 'subscribe';
            $rootScope.url = window.location.pathname + '#/subscribe';
            $scope.selectedPrice = Process.selectedPrice ? Process.selectedPrice : Process.artist.month_contribution;
            Process.selectedPrice = $scope.selectedPrice;
            Process.currentLanguage = $scope.language;
            Process.shareUrl = $location.absUrl();
            Process.shareUrl = Process.shareUrl.substr(0, Process.shareUrl.indexOf('#'));
            $scope.artistSubscribeTxt = $sce.trustAsHtml(Process.artist.subscribe_txt);

            $scope.subscribeMsg1 = interpolate(
                gettext('You will be subscribing to support %s for'),
                [$scope.artist.name]
            );

            $scope.subscribeMsg2 = interpolate(
                gettext('%s per month*:'),
                [$filter('frenchCurrency')($scope.selectedPrice)]
            );
        };

        $scope.loadData();
    }
]);

ghosterControllers.controller('FormatCtrl', ['$rootScope', '$scope', '$window', 'Process',
    function($rootScope, $scope, $window, Process) {
        $rootScope.activateSection(1);

        $scope.loadData = function() {
            // Check if we are following the process
            if(Process.artist && Process.selectedProduct && Process.type && Process.selectedPrice) {
                // We are, set some values
                $scope.init();
            } else {
                // Redirect to home
                $window.location.href = "#/";
            }
        };

        $scope.init = function() {
            $scope.type = Process.type;

            // If the format is already chosen
            if(Process.selectedFormat) {
                // Find it in the list
                angular.forEach(Process.selectedProduct.formats, function(format) {
                    if(format.id === Process.selectedFormat.id) {
                        $scope.selectedFormat = format;
                    }
                });
            } else {
                // Init it
                $scope.selectedFormat = Process.selectedProduct.formats[0];
            }

            Process.selectedFormat = $scope.selectedFormat;
            $scope.selectedProduct = Process.selectedProduct;
        };

        $scope.$watch('selectedFormat', function(newValue, oldValue) {
            if(newValue !== oldValue) {
                Process.selectedFormat = newValue;
            }
        });

        $scope.loadData();
    }
]);

ghosterControllers.controller('ConfirmationPWYWCtrl', ['$rootScope', '$scope', '$window', 'Process', 'Customer',
    function($rootScope, $scope, $window, Process, Customer) {
        $rootScope.sections[2].active = true;

        $scope.formData = {
            first_name: "",
            last_name: "",
            email: "",
            confEmail: "",
            city: Process.maxmind.city.names[Process.currentLanguage],
            country: Process.maxmind.country.names[Process.currentLanguage],
            state: Process.maxmind.subdivisions && Process.maxmind.subdivisions.length ? Process.maxmind.subdivisions[0].names[Process.currentLanguage] : "",
            zip: Process.maxmind.postal_code ? Process.maxmind.postal_code : "",
            cardNumber: "",
            cardExpiryMonth: "",
            cardExpiryYear: "",
            cardCvc: "",
            terms: false,
            language: Process.currentLanguage,
            stripeToken: "",
            latitude: Process.maxmind.location.latitude,
            longitude: Process.maxmind.location.longitude,
            country_iso_code: Process.maxmind.country.iso_code,
            subdivision_iso_code: Process.maxmind.subdivisions && Process.maxmind.subdivisions.length ? Process.maxmind.subdivisions[0].iso_code : "",
            maxmind: Process.maxmind
        };

        $scope.formErrors = "";

        // this identifies your website in the createToken call below
        Stripe.setPublishableKey(Process.artist.stripe_pk);

        $scope.loadData = function() {
            // Check if we are following the process
            if(Process.artist && Process.type && Process.selectedPrice && Process.selectedFormat && Process.selectedProduct) {
                // We are, set some values
                $scope.init();
            } else {
                // Redirect to home
                $window.location.href = "#/";
            }
        };

        $scope.init = function() {
            $scope.type = Process.type;
            $scope.selectedProduct = Process.selectedProduct;
            $scope.selectedPrice = Process.selectedPrice;
            $scope.selectedArtist = Process.artist;
        };

        $scope.stripeResponseHandler = function(status, response) {
            if (response.error) {
                $('.loading').fadeOut();
                // Show the errors on the form

                $scope.formErrors = response.error.message;

                $scope.$apply();

                // Disable the submit button to prevent repeated clicks
                //$($event.currentTarget).find('button').prop('disabled', false);
            } else {
                // token contains id, last4, and card type
                var token = response.id;

                $scope.formData.stripeToken = token;

                // Create customer
                $scope.createCustomer();
            }
        };

        $scope.createStripeToken = function() {
            Stripe.createToken({
                number: $scope.formData.cardNumber,
                cvc: $scope.formData.cardCvc,
                exp_month: $scope.formData.cardExpiryMonth,
                exp_year: $scope.formData.cardExpiryYear
            }, $scope.stripeResponseHandler);
        };

        $scope.submitForm = function($event) {
            $('.loading').fadeIn();
            if($scope.validateForm()) {
                // Disable the submit button to prevent repeated clicks
                $($event.currentTarget).find('button').prop('disabled', true);

                if(Process.selectedPrice > 0)
                    $scope.createStripeToken();
                else
                    $scope.createCustomer();
            }
        };

        $scope.validateForm = function() {
            if($scope.formData.email !== $scope.formData.confEmail) {
                $('.loading').fadeOut();
                $scope.formErrors = gettext("Emails don't match.");
                return false;
            }

            Process.email = $scope.formData.email;

            return true;
        };

        $scope.createCustomer = function() {
            $scope.formData.price = Process.selectedPrice;
            $scope.formData.total = $scope.formData.price;
            $scope.formData.type = Process.type;
            $scope.formData.choix = Process.selectedProduct.id;
            $scope.formData.format = Process.selectedFormat.id;
            $scope.formData.artist = Process.artist.id;

            var cust = new Customer($scope.formData);
            cust.$save()
                .then(function(data) {
                    $window.location.href = '#/end';
                })
                .catch(function(data) {
                    $('.loading').fadeOut();
                    $scope.formErrors = gettext("Error while saving data: ") + data.data;
                });
        };

        angular.element(document).ready(function () {
            // Init tooltip
            if($('.trgTooltip').length > 0)
                $('.trgTooltip').tooltip();
        });

        $scope.loadData();
    }
]);

ghosterControllers.controller('ConfirmationSubscribeCtrl', ['$rootScope', '$scope', '$window', 'Process', 'Customer',
    function($rootScope, $scope, $window, Process, Customer) {
        $rootScope.sections[2].active = true;

        $scope.formData = {
            first_name: "",
            last_name: "",
            email: "",
            confEmail: "",
            city: Process.maxmind.city.names[Process.currentLanguage],
            country: Process.maxmind.country.names[Process.currentLanguage],
            state: Process.maxmind.subdivisions && Process.maxmind.subdivisions.length ? Process.maxmind.subdivisions[0].names[Process.currentLanguage] : "",
            zip: Process.maxmind.postal_code ? Process.maxmind.postal_code : "",
            cardNumber: "",
            cardExpiryMonth: "",
            cardExpiryYear: "",
            cardCvc: "",
            terms: false,
            language: Process.currentLanguage,
            stripeToken: "",
            latitude: Process.maxmind.location.latitude,
            longitude: Process.maxmind.location.longitude,
            country_iso_code: Process.maxmind.country.iso_code,
            subdivision_iso_code: Process.maxmind.subdivisions && Process.maxmind.subdivisions.length ? Process.maxmind.subdivisions[0].iso_code : "",
            maxmind: Process.maxmind
        };

        $scope.formErrors = "";

        // this identifies your website in the createToken call below
        Stripe.setPublishableKey(Process.artist.stripe_pk);

        $scope.loadData = function() {
            // Check if we are following the process
            if(Process.artist && Process.type && Process.selectedPrice) {
                // We are, set some values
                $scope.init();
            } else {
                // Redirect to home
                $window.location.href = "#/";
            }
        };

        $scope.init = function() {
            $scope.type = Process.type;
            $scope.selectedPrice = Process.selectedPrice;
            $scope.selectedArtist = Process.artist;
        };

        $scope.stripeResponseHandler = function(status, response) {
            if (response.error) {
                $('.loading').fadeOut();
                // Show the errors on the form

                $scope.formErrors = response.error.message;

                $scope.$apply();

                // Disable the submit button to prevent repeated clicks
                //$($event.currentTarget).find('button').prop('disabled', false);
            } else {
                // token contains id, last4, and card type
                var token = response.id;

                $scope.formData.stripeToken = token;

                // Create customer
                $scope.createCustomer();
            }
        };

        $scope.createStripeToken = function() {
            Stripe.createToken({
                number: $scope.formData.cardNumber,
                cvc: $scope.formData.cardCvc,
                exp_month: $scope.formData.cardExpiryMonth,
                exp_year: $scope.formData.cardExpiryYear
            }, $scope.stripeResponseHandler);
        };

        $scope.submitForm = function($event) {
            $('.loading').fadeIn();
            if($scope.validateForm()) {
                // Disable the submit button to prevent repeated clicks
                $($event.currentTarget).find('button').prop('disabled', true);

                if(Process.selectedPrice > 0)
                    $scope.createStripeToken();
                else
                    $scope.createCustomer();
            }
        };

        $scope.validateForm = function() {
            if($scope.formData.email !== $scope.formData.confEmail) {
                $('.loading').fadeOut();
                $scope.formErrors = gettext("Emails don't match.");
                return false;
            }

            Process.email = $scope.formData.email;

            return true;
        };

        $scope.createCustomer = function() {
            $scope.formData.price = Process.selectedPrice;
            $scope.formData.total = $scope.formData.price;
            $scope.formData.type = Process.type;
            $scope.formData.artist = Process.artist.id;

            var cust = new Customer($scope.formData);
            cust.$save()
                .then(function(data) {
                    $window.location.href = '#/end';
                })
                .catch(function(data) {
                    $('.loading').fadeOut();
                    $scope.formErrors = gettext("Error while saving data: ") + data.data;
                });
        };

        angular.element(document).ready(function () {
            // Init tooltip
            if($('.trgTooltip').length > 0)
                $('.trgTooltip').tooltip();

            // Init placeholders
            $('input, textarea').placeholder();
        });

        $scope.loadData();
    }
]);

ghosterControllers.controller('EndCtrl', ['$rootScope', '$scope', 'Process', 'TPS', 'TVQ', 'URLShortener',
    function($rootScope, $scope, Process, TPS, TVQ, URLShortener) {
        $rootScope.sections[3].active = true;

        $scope.shareTxt = interpolate(
            gettext("I chose to support %s's career with a contribution of "),
            [Process.artist.name]
        );
        $scope.shareMonthTxt = gettext(" per month");

        $scope.selectedPrice = Process.selectedPrice;
        $scope.ratio = 1 + ((1 * TPS) + (1 * TVQ));
        $scope.total_without_taxes = $scope.selectedPrice / $scope.ratio;
        $scope.tps = $scope.total_without_taxes * TPS;
        $scope.tvq = $scope.total_without_taxes * TVQ;

        console.log(Process);
        $scope.email = Process.email;

        // Get shortened URL
        $scope.shortURL = '';
        var temp = new URLShortener();
        temp.longUrl = Process.shareUrl;
        temp.$save(function(response) {
            $scope.shortURL = response.id;

            if(Process.type === 'pwyw') {
                $scope.shares = [
                    {type:'twitter', link:'http://twitter.com/share?text='+$scope.shareTxt+$scope.selectedPrice+'$'+'&url='+$scope.shortURL+'&referrer=REFERRER'},
                    {type:'facebook', link:'https://www.facebook.com/dialog/feed?app_id=157823707728026&link='+Process.shareUrl+'&picture=https://ghostercontent.s3.amazonaws.com/dist/img/logo.png&name='+$scope.shareTxt+$scope.selectedPrice+'$'+'&caption=&description='+$scope.shortURL+'&redirect_uri=https://www.ghoster.me'},
                ];
            } else {
                $scope.shares = [
                    {type:'twitter', link:'http://twitter.com/share?text='+$scope.shareTxt+$scope.selectedPrice+'$'+$scope.shareMonthTxt+'&url='+$scope.shortURL+'&referrer=SHARE'},
                    {type:'facebook', link:'https://www.facebook.com/dialog/feed?app_id=157823707728026&link='+Process.shareUrl+'&picture=https://ghostercontent.s3.amazonaws.com/dist/img/logo.png&name='+$scope.shareTxt+$scope.selectedPrice+'$'+$scope.shareMonthTxt+'&caption=&description='+$scope.shortURL+'&redirect_uri=https://www.ghoster.me'},
                ];
            }

            Process = {};
        });
    }
]);
