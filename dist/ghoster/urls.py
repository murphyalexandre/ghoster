from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from django.views.i18n import javascript_catalog
import views

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/v1/artists', include('artist.urls', namespace='artist')),
    url(r'^api/v1/products', include('product.urls', namespace='product')),
    url(r'^api/v1/customers', include('customer.urls', namespace='customer')),
    url(r'^api/v1/', views.api_root, name='api_root'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^ckeditor/', include('ckeditor.urls')),

    # Translations
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^jsi18n/$', javascript_catalog, {'packages': ('ghoster.me',),}),

    # Download bought product
    url(r'^download/(?P<hash_param>[\w-]+)', views.download, name="download"),

    # Unsubscribe page
    url(r'^unsubscribe/(?P<hash_param>[\w-]*)', views.unsubscribe, name="unsubscribe"),

    # Exclusive page
    url(r'^exclusive/(?P<h>[\w-]*)', views.exclusive, name="exclusive"),

    # Partials pages
    url(r'^pay/', views.pay, name="pay"),
    url(r'^subscribe/', views.subscribe, name="subscribe"),
    url(r'^format/', views.format, name="format"),
    url(r'^confirmationpwyw/', views.confirmationpwyw, name="confirmationpwyw"),
    url(r'^confirmationsubscribe/', views.confirmationsubscribe, name="confirmationsubscribe"),
    url(r'^end/', views.end, name="end"),

    url(r'^thankyou/', views.thankyou, name="thankyou"),

    # Static pages
    url(r'^(?P<artist>[\w-]+)/faqpwyw/', views.faqpwyw, name="faqpwyw"),
    url(r'^(?P<artist>[\w-]+)/faqsubscribe/', views.faqsubscribe, name="faqsubscribe"),
    url(r'^(?P<artist>[\w-]+)/terms/', views.terms, name="terms"),
    url(r'^(?P<artist>[\w-]+)/help/', views.help, name="help"),
    url(r'^(?P<artist>[\w-]+)/why/', views.why, name="why"),

    # We not using the routing from django.
    url(r'^(?P<artist>[\w-]+)/$', views.app, name="app"),
    url(r'^(?P<artist>[\w-]+)/(?P<album>[\w-]+)/$', views.app, name="app"),

    url(r'^$', views.index, name="index"),
)

# Serve static content from Django when in debug mode
if settings.DEBUG:
    static_url = settings.STATIC_URL
    if len(static_url) > 1 and static_url.startswith('/'):
        static_url = static_url[1:]
    urlpatterns = patterns('',
        (r'^%s(?P<path>.*)$' % static_url, 'django.views.static.serve', { 'document_root': settings.STATIC_ROOT }),) + urlpatterns
