from __future__ import absolute_import
from celery import shared_task


@shared_task
def update_courrielleur():
    from django.core.management import call_command

    call_command('update_courrielleur')


@shared_task
def send_email(pk):
    from django.conf import settings
    from customer.models import Customer
    from artist.courrielleur import Courrielleur
    from django.template import Context, loader
    from decimal import Decimal
    from django.utils.encoding import smart_bytes
    from django.utils.translation import ugettext as _
    from django.utils import translation

    obj = Customer.objects.get(pk=pk)

    # Simple tax calc
    ratio = 1 + ((1 * settings.TAXE_TPS) + (1 * settings.TAXE_TVQ))
    total_without_taxes = obj.price / Decimal(ratio)
    tps = total_without_taxes * Decimal(settings.TAXE_TPS)
    tvq = total_without_taxes * Decimal(settings.TAXE_TVQ)
    TWOPLACES = Decimal(10) ** -2

    # Activate language before getting the template
    prev_language = translation.get_language()
    translation.activate(obj.language)

    try:
        # Load template and build data for chosen type
        template_file = ''
        context = False
        if obj.type == 'pwyw':
            template_file = 'templated_email/download.html'
            context = Context({
                'artist': obj.artist.name,
                'first_name': obj.first_name,
                'price': obj.price,
                'choix': obj.choix.name,
                'format': obj.format.name,
                'download': obj.download_link,
                'tps': tps.quantize(TWOPLACES),
                'tvq': tvq.quantize(TWOPLACES),
                })
        else:
            template_file = 'templated_email/subscribe.html'
            context = Context({
                'artist': obj.artist.name,
                'first_name': obj.first_name,
                'price': obj.price,
                'tps': tps.quantize(TWOPLACES),
                'tvq': tvq.quantize(TWOPLACES),
                'exclusive_link': settings.BASE_URL+'exclusive/'+obj.download,
                'email': obj.email,
                })

        # Generate email with template
        template = loader.get_template(template_file)
        content = template.render(context)
        c = Courrielleur()
        c.email = obj.email
        c.encoding = u'utf-8'
        c.subject = _('Transaction Confirmation')
        c.sender_email = u'donotreply@ghoster.me'
        c.sender_name = obj.artist.name

        # Convert explicitly to bytes
        c.html_message = smart_bytes(content)

        c.relay_send()

        # Add user to mailing list
        c = Courrielleur()
        c.client_id = obj.artist.courrielleur_id
        c.list_id = obj.artist.courrielleur_pwywlist_id \
            if obj.type == 'pwyw' \
            else obj.artist.courrielleur_subscribelist_id
        c.email = obj.email
        c.set_data('Prenom', smart_bytes(obj.first_name))
        c.set_data('Nom', smart_bytes(obj.last_name))
        c.set_data('Langue', obj.language)
        c.set_data('Type', obj.type)
        c.set_data('Total', obj.price)
        c.set_data('Duree', "0")
        c.set_data('Partage', "0")
        c.set_data('Partageconfirme', "0")
        c.set_data('TrakingID', obj.download)
        c.set_data('Algorithme', "0")
        c.set_data('Ville', smart_bytes(obj.city))
        c.set_data('Province', smart_bytes(obj.state))
        c.set_data('Pays', smart_bytes(obj.country))
        c.set_data('Etat', "")
        c.set_data('CodePostal', smart_bytes(obj.zip))
        c.set_data('fsa', smart_bytes(obj.zip[:-3]))
        c.set_data('IP', smart_bytes(obj.ip))
        c.set_data('IsoPays', smart_bytes(obj.country_iso_code))
        c.set_data('IsoProvince', smart_bytes(obj.subdivision_iso_code))
        c.set_data('Latitude', smart_bytes(obj.latitude))
        c.set_data('longitude', smart_bytes(obj.longitude))
        c.subscribe()
    finally:
        translation.activate(prev_language)

    return True
