from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from customer import views

urlpatterns = patterns('customer.views',
    url(r'^$', views.CustomerList.as_view(), name='list'),
    url(r'^/(?P<pk>[0-9]+)$', views.CustomerDetail.as_view(), name='detail'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
