from django.db import models
from product.models import Product, ProductFormat
from artist.models import Artist

# Create your models here.
class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField(max_length=512)
    city = models.CharField(max_length=200)
    state = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    zip = models.CharField(max_length=10)
    stripe_cust = models.CharField(max_length=255, null=True)
    language = models.CharField(max_length=2)
    mailing = models.BooleanField(default=True)
    price = models.DecimalField(max_digits=5, decimal_places=2)
    shipping = models.DecimalField(max_digits=5, decimal_places=2, null=True)
    type = models.CharField(max_length=10)
    ip = models.GenericIPAddressField()
    download = models.CharField(max_length=255, null=True)
    download_link = models.CharField(max_length=512, null=True)
    downloaded = models.BooleanField(default=False)
    stripe_plan = models.CharField(max_length=255, null=True)
    artist = models.ForeignKey(Artist)
    choix = models.ForeignKey(Product, null=True)
    format = models.ForeignKey(ProductFormat, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    country_iso_code = models.CharField(max_length=10, null=True)
    subdivision_iso_code = models.CharField(max_length=10, null=True)
    latitude = models.FloatField("Latitude", blank=True, null=True)
    longitude = models.FloatField("Longtitude", blank=True, null=True)

    def __unicode__(self):
        return self.first_name + u" " + self.last_name
