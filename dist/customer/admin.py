from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from customer.models import Customer
from customer.resources import CustomerResource
from django.conf import settings
from boto.s3.connection import S3Connection
from django.utils.encoding import smart_bytes
import hashlib
import time

# Register your models here.
class CustomerAdmin(ImportExportModelAdmin):
    resource_class = CustomerResource
    readonly_fields = ('created_at',)
    list_display = ('email', 'first_name', 'last_name', 'city', 'state', 'country', 'zip', 'type')
    search_fields = ['first_name', 'last_name', 'email', 'city', 'state', 'country', 'zip', 'stripe_cust', 'language', 'mailing', 'price', 'shipping', 'type', 'ip', 'download', 'download_link', 'downloaded', 'stripe_plan', 'artist__name', 'choix__name', 'format__name']
    actions = ['regenerate_download', 'regenerate_hash']

    def regenerate_hash(self, request, queryset):
        nb_updated = 0
        for customer in queryset:
            # Generate unique hash for some uses
            first_name = customer.first_name
            last_name = customer.last_name
            choice = customer.choix.name if customer.type == 'pwyw' else 'subscribe'
            format = customer.format.name if customer.type == 'pwyw' else 'subscribe'
            email = customer.email
            current_time = unicode(time.time())
            download_str = first_name + last_name + choice + u'_' + format + email + current_time
            customer.download = hashlib.sha224(smart_bytes(download_str)).hexdigest()
            customer.save()
            nb_updated += 1

        if nb_updated == 1:
            message_bit = "1 customer hash was"
        else:
            message_bit = "%s customers hashes were" % nb_updated
        self.message_user(request, "%s regenerated." % message_bit)

    def regenerate_download(self, request, queryset):
        nb_updated = 0
        for customer in queryset:
            # generate link only for pwyw
            if customer.type == 'pwyw':
                # Generate download link using signed url by amazon s3
                c = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
                customer.download_link = c.generate_url(expires_in=long(settings.AWS_LINK_EXPIRATION), method='GET', bucket=settings.AWS_STORAGE_BUCKET_NAME, key=customer.format.link, query_auth=True, force_http=False)
                customer.save()
                nb_updated += 1

        if nb_updated == 1:
            message_bit = "1 customer was"
        else:
            message_bit = "%s customers were" % nb_updated
        self.message_user(request, "%s download links were regenerated." % message_bit)

admin.site.register(Customer, CustomerAdmin)
