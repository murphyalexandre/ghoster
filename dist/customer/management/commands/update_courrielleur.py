from django.core.management.base import BaseCommand
from customer.models import Customer
from artist.models import Artist
from artist.courrielleur import Courrielleur
import datetime

def diff_month(d1, d2):
    return (d1.year - d2.year)*12 + d1.month - d2.month

class Command(BaseCommand):
    help = "Update all customer courrielleur info."

    def handle(self, *args, **options):
        c = Courrielleur()

        # Load each list
        artists = Artist.objects.all()
        artists_courrielleur_pwyw = dict()
        artists_courrielleur_subscribe = dict()
        for artist in artists:
            # PWYW
            c.client_id = artist.courrielleur_id
            c.list_id = artist.courrielleur_pwywlist_id
            artists_courrielleur_pwyw[str(artist.id)] = c.list_show()['data']['records']

            # PWYW
            c.client_id = artist.courrielleur_id
            c.list_id = artist.courrielleur_subscribelist_id
            artists_courrielleur_subscribe[str(artist.id)] = c.list_show()['data']['records']

        # UPDATE PWYW
        now = datetime.datetime.now()
        print u"Starting update of PWYW customers.\n"
        for artist_id in artists_courrielleur_pwyw:
            for customer in artists_courrielleur_pwyw[artist_id]:
                print u'Updating customer: %s' % (customer['email'])
                customer_found = Customer.objects.filter(artist__id__exact=int(artist_id), email__iexact=customer['email'])
                if not customer_found:
                    print u'No customer found.\n'
                    continue

                # Get the first record
                customer_found = customer_found[0]

                date_diff = now - customer_found.created_at.replace(tzinfo=None)
                months_diff = diff_month(now, customer_found.created_at.replace(tzinfo=None))
                months_diff = 1 if months_diff <= 0 else months_diff

                c.client_id = customer_found.artist.courrielleur_id
                c.list_id = customer_found.artist.courrielleur_subscribelist_id
                c.record_id = customer['id']
                c.set_data('Total', customer_found.price if customer_found.type == 'pwyw' else customer_found.price * months_diff)
                c.set_data('Duree', date_diff.days)
                #c.set_data("Partage", $PWYWres['partage'])
                #c.set_data("Partageconfirme", $PWYWres['partage_confirme'])
                c.update_record()

                print 'Done.\n'
