# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Product.name_en'
        db.add_column(u'product_product', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Product.name_fr'
        db.add_column(u'product_product', 'name_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ProductFormat.name_en'
        db.add_column(u'product_productformat', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ProductFormat.name_fr'
        db.add_column(u'product_productformat', 'name_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ProductFormat.description_en'
        db.add_column(u'product_productformat', 'description_en',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ProductFormat.description_fr'
        db.add_column(u'product_productformat', 'description_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ProductFormat.help_link_en'
        db.add_column(u'product_productformat', 'help_link_en',
                      self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True),
                      keep_default=False)

        # Adding field 'ProductFormat.help_link_fr'
        db.add_column(u'product_productformat', 'help_link_fr',
                      self.gf('django.db.models.fields.CharField')(max_length=500, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Product.name_en'
        db.delete_column(u'product_product', 'name_en')

        # Deleting field 'Product.name_fr'
        db.delete_column(u'product_product', 'name_fr')

        # Deleting field 'ProductFormat.name_en'
        db.delete_column(u'product_productformat', 'name_en')

        # Deleting field 'ProductFormat.name_fr'
        db.delete_column(u'product_productformat', 'name_fr')

        # Deleting field 'ProductFormat.description_en'
        db.delete_column(u'product_productformat', 'description_en')

        # Deleting field 'ProductFormat.description_fr'
        db.delete_column(u'product_productformat', 'description_fr')

        # Deleting field 'ProductFormat.help_link_en'
        db.delete_column(u'product_productformat', 'help_link_en')

        # Deleting field 'ProductFormat.help_link_fr'
        db.delete_column(u'product_productformat', 'help_link_fr')


    models = {
        u'artist.artist': {
            'Meta': {'object_name': 'Artist'},
            'courrielleur_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_pwywlist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_subscribelist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_contribution': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'stripe_id': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'artist': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['artist.Artist']"}),
            'category': ('django.db.models.fields.CharField', [], {'default': "'album'", 'max_length': '5'}),
            'cover': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'default_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'min_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'step_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'})
        },
        u'product.productformat': {
            'Meta': {'object_name': 'ProductFormat'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'description_fr': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'help_link': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'help_link_en': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'help_link_fr': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'formats'", 'to': u"orm['product.Product']"})
        }
    }

    complete_apps = ['product']