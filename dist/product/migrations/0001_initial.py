# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Product'
        db.create_table(u'product_product', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('category', self.gf('django.db.models.fields.CharField')(default='album', max_length=5)),
            ('default_price', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            ('min_price', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            ('max_price', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
            ('step_price', self.gf('django.db.models.fields.DecimalField')(max_digits=5, decimal_places=2)),
        ))
        db.send_create_signal(u'product', ['Product'])

        # Adding model 'ProductFormat'
        db.create_table(u'product_productformat', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('product', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['product.Product'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('link', self.gf('django.db.models.fields.URLField')(max_length=500)),
        ))
        db.send_create_signal(u'product', ['ProductFormat'])


    def backwards(self, orm):
        # Deleting model 'Product'
        db.delete_table(u'product_product')

        # Deleting model 'ProductFormat'
        db.delete_table(u'product_productformat')


    models = {
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'category': ('django.db.models.fields.CharField', [], {'default': "'album'", 'max_length': '5'}),
            'default_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'min_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'step_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'})
        },
        u'product.productformat': {
            'Meta': {'object_name': 'ProductFormat'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.URLField', [], {'max_length': '500'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['product.Product']"})
        }
    }

    complete_apps = ['product']