from modeltranslation.translator import translator, TranslationOptions
from product.models import Product, ProductFormat

class ProductTranslationOptions(TranslationOptions):
    fields = ('name',)

translator.register(Product, ProductTranslationOptions)

class ProductFormatTranslationOptions(TranslationOptions):
    fields = ('name', 'description', 'help_link')

translator.register(ProductFormat, ProductFormatTranslationOptions)
