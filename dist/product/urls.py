from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from product import views

urlpatterns = patterns('product.views',
    url(r'^$', views.ProductList.as_view(), name='list'),
    url(r'^/(?P<pk>[0-9]+)$', views.ProductDetail.as_view(), name='detail'),
    url(r'^/(?P<pk>[0-9]+)/formats$', views.ProductFormatList.as_view(), name='format-list'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
