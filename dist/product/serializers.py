from rest_framework import serializers
from product.models import Product, ProductFormat

class ProductFormatSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductFormat
        fields = ('id', 'product', 'name', 'description', 'help_link')

class ProductSerializer(serializers.ModelSerializer):
    formats = ProductFormatSerializer(many=True)

    class Meta:
        model = Product
        fields = ('id', 'name', 'category', 'cover', 'default_price', 'min_price', 'max_price', 'step_price', 'formats', 'can_be_free')
