import os
from django.db import models
from artist.models import Artist
from django.utils.translation import ugettext_lazy as _

# Create your models here.
class Product(models.Model):
	artist = models.ForeignKey(Artist, related_name='products')
	name = models.CharField(max_length=200)

	ALBUM = 'album'
	EP = 'ep'
	SINGLE = 'single'
	VIDEO = 'video'
	BOOK = 'book'
	PHOTOS = 'photos'
	LOGOS = 'logos'
	TECH = 'tech'
	THREED = '3d'
	TEXT = 'text'
	TABLE = 'table'
	SLIDE = 'slideshow'
	MAP = 'map'
	DATABASE = 'db'
	CLOSED_SOURCE = 'closed_source'
	OPEN_SOURCE = 'open_source'
	SOFTWARE = 'software'
	CATEGORY_CHOICES = (
		(ALBUM, _('Album')),
		(EP, _('EP')),
		(SINGLE, _('Single')),
		(VIDEO, _('Video')),
		(BOOK, _('Book')),
		(PHOTOS, _('Photos')),
		(LOGOS, _('Logos')),
		(TECH, _('Technical Drawing')),
		(THREED, _('3D Plan')),
		(TEXT, _('Text Document')),
		(TABLE, _('Table')),
		(SLIDE, _('Slide Show')),
		(MAP, _('Map')),
		(DATABASE, _('Database')),
		(CLOSED_SOURCE, _('Closed Source Code')),
		(OPEN_SOURCE, _('Open Source Code')),
		(SOFTWARE, _('Software')),
	)
	category = models.CharField(max_length=30,
								choices=CATEGORY_CHOICES,
								default=ALBUM)

	default_price = models.DecimalField(max_digits=5, decimal_places=2)
	min_price = models.DecimalField(max_digits=5, decimal_places=2)
	max_price = models.DecimalField(max_digits=5, decimal_places=2)
	step_price = models.DecimalField(max_digits=5, decimal_places=2)
	can_be_free = models.BooleanField(default=False)

	cover = models.FileField(upload_to='product/covers', null=True)

	slug = models.SlugField(max_length=255)

	def __unicode__(self):
		return unicode(self.name)

class ProductFormat(models.Model):
	product = models.ForeignKey(Product, related_name='formats')
	name = models.CharField(max_length=200)
	description = models.CharField(max_length=200, null=True, blank=True)
	link = models.CharField(u'Download Link', max_length=500)
	help_link = models.CharField(max_length=500, null=True, blank=True)

	def __unicode__(self):
		return unicode(self.name)
