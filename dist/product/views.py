from product.models import Product, ProductFormat
from product.serializers import ProductSerializer, ProductFormatSerializer
from rest_framework import generics, permissions

class ProductList(generics.ListCreateAPIView):
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    # Show all of the PASSENGERS in particular WORKSPACE
    # or all of the PASSENGERS in particular AIRLINE
    def get_queryset(self):
        queryset = Product.objects.all()
        artist = self.request.QUERY_PARAMS.get('artist', None)

        if artist is not None:
            queryset = queryset.filter(artist_id=artist)

        return queryset

class ProductDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

class ProductFormatList(generics.ListCreateAPIView):
    queryset = ProductFormat.objects.all()
    serializer_class = ProductFormatSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
