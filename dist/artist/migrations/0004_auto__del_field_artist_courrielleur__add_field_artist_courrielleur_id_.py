# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Artist.courrielleur'
        db.delete_column(u'artist_artist', 'courrielleur')

        # Adding field 'Artist.courrielleur_id'
        db.add_column(u'artist_artist', 'courrielleur_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'Artist.courrielleur_pwywlist_id'
        db.add_column(u'artist_artist', 'courrielleur_pwywlist_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Adding field 'Artist.courrielleur_subscribelist_id'
        db.add_column(u'artist_artist', 'courrielleur_subscribelist_id',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Artist.courrielleur'
        db.add_column(u'artist_artist', 'courrielleur',
                      self.gf('django.db.models.fields.CharField')(max_length=255, null=True),
                      keep_default=False)

        # Deleting field 'Artist.courrielleur_id'
        db.delete_column(u'artist_artist', 'courrielleur_id')

        # Deleting field 'Artist.courrielleur_pwywlist_id'
        db.delete_column(u'artist_artist', 'courrielleur_pwywlist_id')

        # Deleting field 'Artist.courrielleur_subscribelist_id'
        db.delete_column(u'artist_artist', 'courrielleur_subscribelist_id')


    models = {
        u'artist.artist': {
            'Meta': {'object_name': 'Artist'},
            'courrielleur_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_pwywlist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_subscribelist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_contribution': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_id': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['artist']