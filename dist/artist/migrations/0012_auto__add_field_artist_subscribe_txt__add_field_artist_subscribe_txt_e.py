# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Artist.subscribe_txt'
        db.add_column(u'artist_artist', 'subscribe_txt',
                      self.gf('ckeditor.fields.RichTextField')(default=''),
                      keep_default=False)

        # Adding field 'Artist.subscribe_txt_en'
        db.add_column(u'artist_artist', 'subscribe_txt_en',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.subscribe_txt_fr'
        db.add_column(u'artist_artist', 'subscribe_txt_fr',
                      self.gf('ckeditor.fields.RichTextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Artist.subscribe_txt'
        db.delete_column(u'artist_artist', 'subscribe_txt')

        # Deleting field 'Artist.subscribe_txt_en'
        db.delete_column(u'artist_artist', 'subscribe_txt_en')

        # Deleting field 'Artist.subscribe_txt_fr'
        db.delete_column(u'artist_artist', 'subscribe_txt_fr')


    models = {
        u'artist.artist': {
            'Meta': {'object_name': 'Artist'},
            'courrielleur_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_pwywlist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_subscribelist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'exclusive': ('ckeditor.fields.RichTextField', [], {}),
            'exclusive_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'exclusive_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_pwyw': ('ckeditor.fields.RichTextField', [], {}),
            'faq_pwyw_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_pwyw_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'faq_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help': ('ckeditor.fields.RichTextField', [], {}),
            'help_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_contribution': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'stripe_mode': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stripe_public_live': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_public_test': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_secret_live': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_secret_test': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'subscribe_txt': ('ckeditor.fields.RichTextField', [], {}),
            'subscribe_txt_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'subscribe_txt_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_and_conditions': ('ckeditor.fields.RichTextField', [], {}),
            'terms_and_conditions_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_and_conditions_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'why_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['artist']