from rest_framework import serializers
from artist.models import Artist
from product.serializers import ProductSerializer

class ArtistSerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)
    stripe_pk = serializers.Field(source='stripe_pk')

    class Meta:
        model = Artist
        fields = ('id', 'name', 'stripe_pk', 'month_contribution', 'products', 'slug', 'subscribe_txt')
