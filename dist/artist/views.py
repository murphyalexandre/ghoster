from artist.models import Artist
from artist.serializers import ArtistSerializer
from rest_framework import generics, permissions

class ArtistList(generics.ListCreateAPIView):
    serializer_class = ArtistSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    # Find by name slug
    def get_queryset(self):
        queryset = Artist.objects.all()
        name = self.request.QUERY_PARAMS.get('name', None)

        if name is not None:
            queryset = queryset.filter(slug=name)

        return queryset

class ArtistDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
