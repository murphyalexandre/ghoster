import requests
import urllib
import sys

apiEndPoint = 'https://api.wbsrvc.com/'
apiKey = 'e672e354fc6fcbe4688be017a048ea5b'

class Courrielleur():
    def __init__(self):
        self.user_key = '3fd61259b38d0354c9664f66df6da33a'
        self.data = {}

    def set_data(self, name, value):
        self.data['data['+name+']'] = value

    def get_data(self):
        return self.data

    def send_cmd(self, action):
        dict_data = self.__dict__.copy()
        dict_data.pop("data", None)
        dict_data.update(self.get_data())

        headers = {'apiKey': apiKey}

        str_dict_data = {}
        for k, v in dict_data.iteritems():
            if isinstance(v, unicode):
                v = v.encode('utf8')
            elif isinstance(v, str):
                # Must be encoded in UTF-8
                v = v.decode('utf8')
            str_dict_data[k] = v

        response = requests.post(apiEndPoint+action, headers=headers, data=str_dict_data)

        return response

    def create_client(self):
        return self.send_cmd("Client/Create");

    def relay_send(self):
        return self.send_cmd("Relay/Send");

    def subscribe(self):
        return self.send_cmd("List/SubscribeEmail");

    def unsubscribe(self):
        return self.send_cmd("List/UnsubscribeEmail")

    def list_show(self):
         return self.send_cmd("List/Show");

    def update_record(self):
         return self.send_cmd("List/UpdateRecord");
