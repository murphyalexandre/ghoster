from django.conf.urls import patterns, url
from rest_framework.urlpatterns import format_suffix_patterns
from artist import views

urlpatterns = patterns('artist.views',
    url(r'^$', views.ArtistList.as_view(), name='list'),
    url(r'^/(?P<pk>[0-9]+)$', views.ArtistDetail.as_view(), name='detail'),
)

urlpatterns = format_suffix_patterns(urlpatterns)
