module.exports = function(grunt) {

  /**
   * Load in our build configuration file.
   */
  var userConfig = require( './build.config.js' );

  var taskConfig = {
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      js: {
        src: [
          '<%= vendor_files.build.js %>',
          '<%= app_files.build.js %>',
        ],
        dest: 'ghoster/static/dist/js/concat.js'
      },
      css: {
        src: [
          '<%= vendor_files.build.css %>',
          '<%= app_files.build.css %>',
        ],
        dest: 'ghoster/static/dist/css/concat.css'
      },
    },
    rename: {
      css: {
        files: [
          {src: ['ghoster/static/dist/css/concat.css'], dest: ['ghoster/static/dist/css/min.css']},
        ]
      },
      js: {
        files: [
          {src: ['ghoster/static/dist/js/concat.js'], dest: ['ghoster/static/dist/js/min.js']},
        ]
      }
    },
    uglify: {
      options: {
        mangle: true,
        report: 'gzip',
        compress: {
          drop_console: true
        }
      },
      dist: {
        'src': ['ghoster/static/dist/js/concat.js'],
        'dest': 'ghoster/static/dist/js/min.js'
      }
    },
    cssmin: {
      dist: {
        'src': ['ghoster/static/dist/css/concat.css'],
        'dest': 'ghoster/static/dist/css/min.css'
      }
    },
    copy: {
      main: {
        files: [
          {expand: true, cwd: 'ghoster/static/img/', src: ['**'], dest: 'ghoster/static/dist/img/'},
          {expand: true, cwd: 'ghoster/static/fonts/', src: ['**'], dest: 'ghoster/static/dist/fonts/'},
          {expand: true, cwd: 'ghoster/static/js/', src: ['<%= app_files.copy.js %>'], dest: 'ghoster/static/dist/js/'},
          {expand: true, cwd: 'ghoster/static/css/', src: ['<%= app_files.copy.css %>'], dest: 'ghoster/static/dist/css/'},
          {expand: true, cwd: 'ghoster/static/js/vendor/', src: ['<%= vendor_files.copy.js %>'], dest: 'ghoster/static/dist/js/'},
          {expand: true, cwd: 'ghoster/static/css/vendor/', src: ['<%= vendor_files.copy.css %>'], dest: 'ghoster/static/dist/css/'},
        ]
      },
    },
    clean: {
      build: {
        src: ['ghoster/static/dist']
      }
    },
    delta: {
      js: {
         files: ['ghoster/static/js/**/*.js', '!ghoster/static/js/min.js'],
         tasks: ['concat:js', 'rename:js']
      },
      css: {
         files: ['ghoster/static/css/**/*.css', '!ghoster/static/css/min.css'],
         tasks: ['concat:css', 'copy', 'rename:css']
      },
    },
  };

  grunt.initConfig( grunt.util._.extend( taskConfig, userConfig ) );

  grunt.loadNpmTasks('grunt-contrib');
  grunt.loadNpmTasks('grunt-contrib-rename');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-clean');

  // Rename watch task so we can name our watch task watch and build things before launching watch
  grunt.renameTask('watch', 'delta');

  grunt.registerTask('default', ['clean', 'concat', 'rename', 'copy']);
  grunt.registerTask('prod', ['clean', 'concat', 'cssmin', 'uglify', 'copy']);
  grunt.registerTask('watch', ['default', 'delta']);
};
