module.exports = {
  /**
   * This is a collection of file patterns that refer to our app code (the
   * stuff in `src/`). These file paths are used in the configuration of
   * build tasks. `js` is all project javascript, less tests. `ctpl` contains
   * our reusable components' (`src/common`) template HTML files, while
   * `atpl` contains the same, but for our app's code. `html` is just our
   * main HTML file, `less` is our main stylesheet, and `unit` contains our
   * app's unit tests.
   */
  app_files: {
    build: {
      js: [
        'ghoster/static/js/app.js',
        'ghoster/static/js/controllers.js',
        'ghoster/static/js/controllers/*.js',
        'ghoster/static/js/directives.js',
        'ghoster/static/js/directives/*.js',
        'ghoster/static/js/filters.js',
        'ghoster/static/js/services.js',
        'ghoster/static/js/services/*.js',
      ],
      css: [
        'ghoster/static/css/reset.css',
        'ghoster/static/css/ghoster.css',
      ],
    },
    copy: {
      js: [
        'exclusivesetup.js'
      ],
      css: [],
    }
  },

  /**
   * This is the same as `app_files`, except it contains patterns that
   * reference vendor code (`vendor/`) that we need to place into the build
   * process somewhere. While the `app_files` property ensures all
   * standardized files are collected for compilation, it is the user's job
   * to ensure non-standardized (i.e. vendor-related) files are handled
   * appropriately in `vendor_files.js`.
   *
   * The `vendor_files.js` property holds files to be automatically
   * concatenated and minified with our project source files.
   *
   * The `vendor_files.css` property holds any CSS files to be automatically
   * included in our app.
   */
  vendor_files: {
    build: {
      js: [
        'ghoster/static/js/ghoster.js',
        //'ghoster/static/js/vendor/modernizr/modernizr.js',
        //'ghoster/static/js/vendor/html5shiv/dist/html5shiv.js',
        'ghoster/static/js/vendor/bootstrap-tooltip.js',
      ],
      css: [],
      assets: [
      ],
    },
    copy: {
      js: [
        //'html5slider.js',
        'jquery-placeholder/jquery.placeholder.js',
        'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js',
        'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css', // HACK
        'malihu-custom-scrollbar-plugin/mCSB_buttons.png', // HACK
        'html5Forms.js/**/*',
        'jquery-prettyPhoto/js/jquery.prettyPhoto.js',
        'tipsy/src/javascripts/jquery.tipsy.js',
        'jquery-knob/js/jquery.knob.js',
        'jquery-smoothscroll/jquery.smoothscroll.min.js',
        'jquery-waypoints/waypoints.min.js',
      ],
      css: [],
      assets: [],
    }
  },
};
