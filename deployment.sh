#!/bin/sh
grunt prod
#python ghoster/manage.py collectstatic --noinput
rm -rf dist
mkdir dist
cp -R ghoster/* dist
cp requirements.txt dist
cp Procfile dist
rm -rf
rm -rf dist/ghoster/.gitignore
mv dist/ghoster/settings.py.deploy dist/ghoster/settings.py
