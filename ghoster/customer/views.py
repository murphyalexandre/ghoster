from artist.courrielleur import Courrielleur
from artist.models import Artist
from customer.models import Customer
from customer.serializers import CustomerSerializer
from django.conf import settings
from django.utils.decorators import method_decorator
from django.utils.encoding import smart_bytes
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from rest_framework import generics, permissions, status
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.reverse import reverse
import hashlib
import stripe
import time
from boto.s3.connection import S3Connection
import re
from customer.tasks import send_email

class CustomerList(generics.ListCreateAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CustomerList, self).dispatch(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        data = request.DATA

        # Keep the IP in the request
        request.DATA['ip'] = request.META['REMOTE_ADDR']

        # Format zip code if canada
        if request.DATA['country_iso_code'] == 'CA':
            zip_code = request.DATA['zip']
            if re.search(r'^([a-zA-Z][0-9]){3}$', zip_code.strip()):
                request.DATA['zip'] = zip_code.strip().upper()

        if(float(data['price']) > 0):
            if(not data.has_key('stripeToken')):
                return Response(u"Missing Stripe token.", status=status.HTTP_400_BAD_REQUEST)

            # Charge the customer
            # Set your secret key: remember to change this to your live secret key in production
            # See your keys here https://manage.stripe.com/account
            artist = Artist.objects.get(pk=data['artist'])
            stripe.api_key = artist.stripe_secret_live if artist.stripe_mode else artist.stripe_secret_test

            # Get the credit card details submitted by the form
            token = data['stripeToken']

            # Create the charge on Stripe's servers - this will charge the user's card
            plan = False
            try:
                # Create a customer
                customer = stripe.Customer.create(email=data['email'], card=token, description=data['first_name']+' '+data['last_name'])
                request.DATA['stripe_cust'] = customer.id

                if data['type'] == 'pwyw':
                    charge = stripe.Charge.create(customer=customer.id, amount=int(float(data['total']))*100, currency='cad')
                    request.DATA['stripePlan'] = charge.id
                else:
                    # Create subscription plan
                    plan_test = ""
                    if settings.DEBUG:
                        plan_test = "test-"
                    plan_name = "subscribe-"+plan_test+data['price']

                    plan = False
                    try:
                        plan = stripe.Plan.retrieve(plan_name)
                    except stripe.InvalidRequestError, e:
                        # Plan doesn't exist, create it
                        plan = stripe.Plan.create(amount=int(float(data['price']))*100, interval='month', name='Ghoster Subscribe '+data['price'], currency='cad', id=plan_name)

                    # Update existing customer with plan
                    customer.subscriptions.create(plan=plan_name)

                    request.DATA['stripe_plan'] = plan_name
            except stripe.CardError, e:
                # The card has been declined
                return Response(u"Card declined.", status=status.HTTP_400_BAD_REQUEST)

        return super(CustomerList, self).create(request, *args, **kwargs)

    def pre_save(self, obj):
        # Generate unique hash for some uses
        first_name = obj.first_name
        last_name = obj.last_name
        choice = obj.choix.name if obj.type == 'pwyw' else 'subscribe'
        format = obj.format.name if obj.type == 'pwyw' else 'subscribe'
        email = obj.email
        current_time = unicode(time.time())
        download_str = first_name + last_name + choice + u'_' + format + email + current_time
        obj.download = hashlib.sha224(smart_bytes(download_str)).hexdigest()

        # generate link only for pwyw
        if obj.type == 'pwyw':
            # Generate download link using signed url by amazon s3
            c = S3Connection(settings.AWS_ACCESS_KEY_ID, settings.AWS_SECRET_ACCESS_KEY)
            obj.download_link = c.generate_url(expires_in=long(settings.AWS_LINK_EXPIRATION), method='GET', bucket=settings.AWS_STORAGE_BUCKET_NAME, key=obj.format.link, query_auth=True, force_http=False)


    def post_save(self, obj, created=False):
        if created:
            # Save was successful and object was created, we now need to send the confirmation
            # by email plus subscribe them to the mailing list
            # Download email
            send_email.delay(obj.id)

        return super(CustomerList, self).post_save(obj, created)

class CustomerDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    #permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(CustomerList, self).dispatch(request, *args, **kwargs)
