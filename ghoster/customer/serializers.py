from rest_framework import serializers
from customer.models import Customer

class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ('id', 'first_name', 'last_name', 'email', 'city', 'state', 'country', 'zip', 'stripe_cust', 'language', 'mailing', 'price', 'shipping', 'type', 'ip', 'download', 'stripe_plan', 'artist', 'choix', 'format', 'country_iso_code', 'subdivision_iso_code','latitude','longitude')
