from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse
from django.shortcuts import get_object_or_404, render
from artist.models import Artist
from artist.courrielleur import Courrielleur
from product.models import Product
from customer.models import Customer
from django.http import HttpResponse, StreamingHttpResponse
from django.template import Context, RequestContext, loader
import urllib2
from django.utils.encoding import smart_bytes
from django.utils.translation import ugettext as _
import stripe
from django.utils import translation


@api_view(('GET',))
def api_root(request, format=None):
    return Response({
        'customers': reverse('customer:list', request=request, format=format),
        'artists': reverse('artist:list', request=request, format=format),
        'products': reverse('product:list', request=request, format=format),
    })


def index(request):
    return render(request, 'index.html', {})


def thankyou(request):
    return render(request, 'thankyou.html', {})


def app(request, artist, album=None):
    lang = request.GET.get('lang', False)
    if lang:
        # Set default language
        translation.activate(lang)
        request.LANGUAGE_CODE = lang

    a = get_object_or_404(Artist, slug__iexact=artist)

    if album:
        p = get_object_or_404(Product, slug__iexact=album)

    context_data = {}
    context_data['artist'] = a.id

    if album and p:
        context_data['product'] = p.id

    template = loader.select_template([artist+'.html', 'ghoster.html'])
    context = RequestContext(request, context_data)
    return HttpResponse(template.render(context))


def download(request, hash_param):
    # Get customer by hash
    customer = get_object_or_404(Customer, download__exact=hash_param)

    # Check if already downloaded
    if customer.downloaded:
        return render(request, 'download.html', {'message': "File already downloaded"})
    else:
        customer.downloaded = True
        customer.save()

    file_name = customer.format.link.split('/')[-1]
    file_extension = file_name.split('.')[1]
    file_download = urllib2.urlopen(customer.format.link)
    content_type = file_download.headers['content-type']

    response = StreamingHttpResponse(file_download.read(), content_type=content_type)
    response['Content-Disposition'] = 'attachment; filename=%s' % (customer.choix.name + "_" + customer.format.name + "." + file_extension)
    # response["Content-Length"] = os.path.getsize(file_download)
    return response


def unsubscribe(request, hash_param):
    context = {}
    data = request.POST

    if hash_param:
        context['hash_param'] = hash_param
        if data and data['email']:
            context['email'] = data['email']
            # Show list of subscriptions
            customers = Customer.objects.filter(email__exact=data['email'], type__iexact='subscribe')
            context['customers'] = customers

            selection = data.getlist('selection')

            if data and selection:
                context['success'] = True
                context['success_unsubscribe'] = True
                # Unsubscribe from Stripe and Courrielleur for each selection
                for customer_id in selection:
                    # Get customer and artist
                    customer = Customer.objects.get(pk=customer_id)
                    artist = Artist.objects.get(pk=customer.artist.id)

                    try:
                        # Remove from mailing
                        c = Courrielleur()
                        c.client_id = artist.courrielleur_id
                        c.list_id = artist.courrielleur_subscribelist_id
                        c.email = customer.email
                        c.unsubscribe()

                        # Unsub from stripe
                        stripe.api_key = artist.stripe_secret_live if artist.stripe_mode else artist.stripe_secret_test
                        stripe_customer = stripe.Customer.retrieve(customer.stripe_cust)
                        stripe_customer.subscriptions.retrieve(customer.stripe_plan).delete()
                        customer.stripe_plan = "unsubscribed"
                        customer.save()
                    except Exception as e:
                        context['success'] = False
                        context['message'] = '%s (%s)' % (e.message, type(e))
                        return render(request, 'unsubscribe.html', context)

        # Ask customer to enter email again
    elif data and data['email']:
        context['email'] = data['email']

        # Use first customer matching email, we just want to validate his email
        customer = Customer.objects.filter(email__iexact=data['email'], type__iexact='subscribe')
        if customer:
            # Send email to customer with his unsubscribe hash
            context = False
            template_file = 'templated_email/unsubscribe.html'
            context = Context({
                'link': 'https://www.ghoster.me/unsubscribe/%s' % customer[0].download,
                'email': customer[0].email,
                })

            template = loader.get_template(template_file)
            content = template.render(context)
            c = Courrielleur()
            c.email = data['email']
            c.encoding = u'utf-8'
            c.subject = _('Unsubscribe')
            c.sender_email = u'donotreply@ghoster.me'
            c.sender_name = u'Ghoster'

            # Convert explicitly to bytes
            c.html_message = smart_bytes(content)

            c.relay_send()

            # Also send to ghoster
            c.sender_email = u'unsubscribe@ghoster.me'
            c.relay_send()

            context['success'] = True

    return render(request, 'unsubscribe.html', context)


def exclusive(request, h):
    context = {}
    data = request.POST
    if data and data['email']:
        # Check if email and hash are valid
        c = Customer.objects.filter(download=h, email=data['email'])
        if c.exists():
            context['exclusive_content'] = c[0].artist.exclusive
            context['faq_content'] = c[0].artist.faq_subscribe
            context['help_content'] = c[0].artist.help
            context['terms_content'] = c[0].artist.terms_and_conditions
            return render(request, 'exclusive.html', context)
        else:
            return render(request, 'exclusive_error.html', context)

    if h:
        context['hash'] = h
        return render(request, 'exclusive_login.html', context)

    return render(request, 'exclusive_error.html', context)


def faqpwyw(request, artist):
    a = get_object_or_404(Artist, slug__iexact=artist)
    return render(request, 'faqpwyw.html', {'content': a.faq_pwyw})


def faqsubscribe(request, artist):
    a = get_object_or_404(Artist, slug__iexact=artist)
    return render(request, 'faqsubscribe.html', {'content': a.faq_subscribe})


def terms(request, artist):
    a = get_object_or_404(Artist, slug__iexact=artist)
    return render(request, 'terms.html', {'content': a.terms_and_conditions})


def help(request, artist):
    a = get_object_or_404(Artist, slug__iexact=artist)
    return render(request, 'help.html', {'content': a.help})


def why(request, artist):
    a = get_object_or_404(Artist, slug__iexact=artist)
    return render(request, 'why.html', {'content': a.why_subscribe})


def pay(request):
    return render(request, 'partials/pay.html', {})


def subscribe(request):
    return render(request, 'partials/subscribe.html', {})


def format(request):
    return render(request, 'partials/format.html', {})


def confirmationpwyw(request):
    return render(request, 'partials/confirmationpwyw.html', {})


def confirmationsubscribe(request):
    return render(request, 'partials/confirmationsubscribe.html', {})


def end(request):
    return render(request, 'partials/end.html', {})
