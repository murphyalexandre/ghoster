from django.contrib import admin
from artist.models import Artist

# Register your models here.
class ArtistAdmin(admin.ModelAdmin):
    list_display = ('name',)
    search_fields = ['name']
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(Artist, ArtistAdmin)
