# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Artist.stripe_id'
        db.delete_column(u'artist_artist', 'stripe_id')


        # Changing field 'Artist.help'
        db.alter_column(u'artist_artist', 'help', self.gf('ckeditor.fields.RichTextField')())

        # Changing field 'Artist.why_subscribe'
        db.alter_column(u'artist_artist', 'why_subscribe', self.gf('ckeditor.fields.RichTextField')())

        # Changing field 'Artist.terms_and_conditions_fr'
        db.alter_column(u'artist_artist', 'terms_and_conditions_fr', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.faq_subscribe_en'
        db.alter_column(u'artist_artist', 'faq_subscribe_en', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.faq_pwyw_en'
        db.alter_column(u'artist_artist', 'faq_pwyw_en', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.why_subscribe_en'
        db.alter_column(u'artist_artist', 'why_subscribe_en', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.faq_subscribe'
        db.alter_column(u'artist_artist', 'faq_subscribe', self.gf('ckeditor.fields.RichTextField')())

        # Changing field 'Artist.help_en'
        db.alter_column(u'artist_artist', 'help_en', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.faq_pwyw_fr'
        db.alter_column(u'artist_artist', 'faq_pwyw_fr', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.terms_and_conditions'
        db.alter_column(u'artist_artist', 'terms_and_conditions', self.gf('ckeditor.fields.RichTextField')())

        # Changing field 'Artist.faq_subscribe_fr'
        db.alter_column(u'artist_artist', 'faq_subscribe_fr', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.terms_and_conditions_en'
        db.alter_column(u'artist_artist', 'terms_and_conditions_en', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.help_fr'
        db.alter_column(u'artist_artist', 'help_fr', self.gf('ckeditor.fields.RichTextField')(null=True))

        # Changing field 'Artist.faq_pwyw'
        db.alter_column(u'artist_artist', 'faq_pwyw', self.gf('ckeditor.fields.RichTextField')())

        # Changing field 'Artist.why_subscribe_fr'
        db.alter_column(u'artist_artist', 'why_subscribe_fr', self.gf('ckeditor.fields.RichTextField')(null=True))

    def backwards(self, orm):
        # Adding field 'Artist.stripe_id'
        db.add_column(u'artist_artist', 'stripe_id',
                      self.gf('django.db.models.fields.CharField')(default='123123', max_length=200),
                      keep_default=False)


        # Changing field 'Artist.help'
        db.alter_column(u'artist_artist', 'help', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Artist.why_subscribe'
        db.alter_column(u'artist_artist', 'why_subscribe', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Artist.terms_and_conditions_fr'
        db.alter_column(u'artist_artist', 'terms_and_conditions_fr', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.faq_subscribe_en'
        db.alter_column(u'artist_artist', 'faq_subscribe_en', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.faq_pwyw_en'
        db.alter_column(u'artist_artist', 'faq_pwyw_en', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.why_subscribe_en'
        db.alter_column(u'artist_artist', 'why_subscribe_en', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.faq_subscribe'
        db.alter_column(u'artist_artist', 'faq_subscribe', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Artist.help_en'
        db.alter_column(u'artist_artist', 'help_en', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.faq_pwyw_fr'
        db.alter_column(u'artist_artist', 'faq_pwyw_fr', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.terms_and_conditions'
        db.alter_column(u'artist_artist', 'terms_and_conditions', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Artist.faq_subscribe_fr'
        db.alter_column(u'artist_artist', 'faq_subscribe_fr', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.terms_and_conditions_en'
        db.alter_column(u'artist_artist', 'terms_and_conditions_en', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.help_fr'
        db.alter_column(u'artist_artist', 'help_fr', self.gf('django.db.models.fields.TextField')(null=True))

        # Changing field 'Artist.faq_pwyw'
        db.alter_column(u'artist_artist', 'faq_pwyw', self.gf('django.db.models.fields.TextField')())

        # Changing field 'Artist.why_subscribe_fr'
        db.alter_column(u'artist_artist', 'why_subscribe_fr', self.gf('django.db.models.fields.TextField')(null=True))

    models = {
        u'artist.artist': {
            'Meta': {'object_name': 'Artist'},
            'courrielleur_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_pwywlist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_subscribelist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'faq_pwyw': ('ckeditor.fields.RichTextField', [], {}),
            'faq_pwyw_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_pwyw_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'faq_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help': ('ckeditor.fields.RichTextField', [], {}),
            'help_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_contribution': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'terms_and_conditions': ('ckeditor.fields.RichTextField', [], {}),
            'terms_and_conditions_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_and_conditions_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'why_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['artist']