# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Artist.faq_subscribe_en'
        db.add_column(u'artist_artist', 'faq_subscribe_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.faq_subscribe_fr'
        db.add_column(u'artist_artist', 'faq_subscribe_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.terms_and_conditions_en'
        db.add_column(u'artist_artist', 'terms_and_conditions_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.terms_and_conditions_fr'
        db.add_column(u'artist_artist', 'terms_and_conditions_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.help_en'
        db.add_column(u'artist_artist', 'help_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.help_fr'
        db.add_column(u'artist_artist', 'help_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.why_subscribe_en'
        db.add_column(u'artist_artist', 'why_subscribe_en',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Artist.why_subscribe_fr'
        db.add_column(u'artist_artist', 'why_subscribe_fr',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Artist.faq_subscribe_en'
        db.delete_column(u'artist_artist', 'faq_subscribe_en')

        # Deleting field 'Artist.faq_subscribe_fr'
        db.delete_column(u'artist_artist', 'faq_subscribe_fr')

        # Deleting field 'Artist.terms_and_conditions_en'
        db.delete_column(u'artist_artist', 'terms_and_conditions_en')

        # Deleting field 'Artist.terms_and_conditions_fr'
        db.delete_column(u'artist_artist', 'terms_and_conditions_fr')

        # Deleting field 'Artist.help_en'
        db.delete_column(u'artist_artist', 'help_en')

        # Deleting field 'Artist.help_fr'
        db.delete_column(u'artist_artist', 'help_fr')

        # Deleting field 'Artist.why_subscribe_en'
        db.delete_column(u'artist_artist', 'why_subscribe_en')

        # Deleting field 'Artist.why_subscribe_fr'
        db.delete_column(u'artist_artist', 'why_subscribe_fr')


    models = {
        u'artist.artist': {
            'Meta': {'object_name': 'Artist'},
            'courrielleur_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_pwywlist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_subscribelist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'faq_pwyw': ('django.db.models.fields.TextField', [], {}),
            'faq_pwyw_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_pwyw_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe': ('django.db.models.fields.TextField', [], {}),
            'faq_subscribe_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'help': ('django.db.models.fields.TextField', [], {}),
            'help_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'help_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_contribution': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'stripe_id': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'terms_and_conditions': ('django.db.models.fields.TextField', [], {}),
            'terms_and_conditions_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_and_conditions_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe': ('django.db.models.fields.TextField', [], {}),
            'why_subscribe_en': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe_fr': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['artist']