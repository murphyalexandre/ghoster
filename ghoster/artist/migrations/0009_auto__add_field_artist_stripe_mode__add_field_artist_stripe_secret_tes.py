# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Artist.stripe_mode'
        db.add_column(u'artist_artist', 'stripe_mode',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Artist.stripe_secret_test'
        db.add_column(u'artist_artist', 'stripe_secret_test',
                      self.gf('django.db.models.fields.CharField')(default='123123', max_length=200),
                      keep_default=False)

        # Adding field 'Artist.stripe_private_test'
        db.add_column(u'artist_artist', 'stripe_private_test',
                      self.gf('django.db.models.fields.CharField')(default='123123', max_length=200),
                      keep_default=False)

        # Adding field 'Artist.stripe_secret_live'
        db.add_column(u'artist_artist', 'stripe_secret_live',
                      self.gf('django.db.models.fields.CharField')(default='123123', max_length=200),
                      keep_default=False)

        # Adding field 'Artist.stripe_private_live'
        db.add_column(u'artist_artist', 'stripe_private_live',
                      self.gf('django.db.models.fields.CharField')(default='123123', max_length=200),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Artist.stripe_mode'
        db.delete_column(u'artist_artist', 'stripe_mode')

        # Deleting field 'Artist.stripe_secret_test'
        db.delete_column(u'artist_artist', 'stripe_secret_test')

        # Deleting field 'Artist.stripe_private_test'
        db.delete_column(u'artist_artist', 'stripe_private_test')

        # Deleting field 'Artist.stripe_secret_live'
        db.delete_column(u'artist_artist', 'stripe_secret_live')

        # Deleting field 'Artist.stripe_private_live'
        db.delete_column(u'artist_artist', 'stripe_private_live')


    models = {
        u'artist.artist': {
            'Meta': {'object_name': 'Artist'},
            'courrielleur_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_pwywlist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_subscribelist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'faq_pwyw': ('ckeditor.fields.RichTextField', [], {}),
            'faq_pwyw_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_pwyw_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'faq_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help': ('ckeditor.fields.RichTextField', [], {}),
            'help_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_contribution': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'stripe_mode': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stripe_private_live': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_private_test': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_secret_live': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_secret_test': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'terms_and_conditions': ('ckeditor.fields.RichTextField', [], {}),
            'terms_and_conditions_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_and_conditions_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'why_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['artist']