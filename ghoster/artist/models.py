from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.
class Artist(models.Model):
    name = models.CharField(max_length=200)
    courrielleur_id = models.CharField('Courrielleur Client ID', max_length=255, null=True)
    courrielleur_pwywlist_id = models.CharField(max_length=255, null=True)
    courrielleur_subscribelist_id = models.CharField(max_length=255, null=True)
    month_contribution = models.DecimalField(max_digits=5, decimal_places=2)
    slug = models.SlugField()
    faq_pwyw = RichTextField()
    faq_subscribe = RichTextField()
    terms_and_conditions = RichTextField()
    help = RichTextField()
    why_subscribe = RichTextField()
    stripe_mode = models.BooleanField('Live', default=False)
    stripe_secret_test = models.CharField(max_length=200)
    stripe_public_test = models.CharField(max_length=200)
    stripe_secret_live = models.CharField(max_length=200)
    stripe_public_live = models.CharField(max_length=200)
    exclusive = RichTextField()
    subscribe_txt = RichTextField()

    @property
    def stripe_pk(self):
        return self.stripe_public_live if self.stripe_mode else self.stripe_public_test

    def __unicode__(self):
        return unicode(self.name)
