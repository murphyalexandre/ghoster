from modeltranslation.translator import translator, TranslationOptions
from artist.models import Artist

class ArtistTranslationOptions(TranslationOptions):
    fields = ('faq_pwyw', 'faq_subscribe', 'terms_and_conditions', 'help', 'why_subscribe', 'exclusive', 'subscribe_txt',)

translator.register(Artist, ArtistTranslationOptions)
