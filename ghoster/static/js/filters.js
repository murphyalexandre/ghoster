'use strict';

/* Directives */
var ghosterFilters = angular.module("ghoster.filters", []);
ghosterFilters.filter('frenchCurrency', [function() {
	return function(price) {
		var comma = ",";
		var negative = price < 0 ? "-" : "";
		var i = parseInt(price = Math.abs(+price || 0).toFixed(2), 10) + "";
		var j = (j = i.length) > 3 ? j % 3 : 0;

		return negative + (j ? i.substr(0, j) + comma : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + comma) + (comma + Math.abs(price - i).toFixed(2).slice(2)) + '$';
	};
}]);
