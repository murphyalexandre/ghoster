'use strict';

// Declare app level module which depends on filters, and services
var ghoster = angular.module(
    'ghoster',
    [
        'ngRoute',
        'ngCookies',

        'ghoster.controllers',
        'ghoster.filters',
        'ghoster.services',
        'ghoster.directives',
    ]
);

ghoster.constant('TVQ', 0.09975);
ghoster.constant('TPS', 0.05);

ghoster.config(['$interpolateProvider', function ($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
}]);

ghoster.config(['$routeProvider', '$httpProvider', '$cookiesProvider',
  function($routeProvider, $httpProvider, $cookiesProvider) {
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';

    $routeProvider.
      when('/pay', {
        templateUrl: '/pay/',
        controller: 'PayCtrl'
      }).
      when('/subscribe', {
        templateUrl: '/subscribe/',
        controller: 'SubscribeCtrl'
      }).
      when('/format', {
        templateUrl: '/format/',
        controller: 'FormatCtrl'
      }).
      when('/confirmationpwyw', {
        templateUrl: '/confirmationpwyw/',
        controller: 'ConfirmationPWYWCtrl'
      }).
      when('/confirmationsubscribe', {
        templateUrl: '/confirmationsubscribe/',
        controller: 'ConfirmationSubscribeCtrl'
      }).
      when('/end', {
        templateUrl: '/end/',
        controller: 'EndCtrl'
      }).
      otherwise({
        redirectTo: '/pay'
      });
  }]);
