from django.contrib import admin
from product.models import Product, ProductFormat

# Register your models here.
class FormatInline(admin.TabularInline):
    model = ProductFormat
    extra = 0

class ProductAdmin(admin.ModelAdmin):
    inlines = [FormatInline]
    list_display = ('name', 'category')
    list_filter = ['artist__name', 'category']
    search_fields = ['name', 'category']
    prepopulated_fields = {"slug": ("name",)}

admin.site.register(Product, ProductAdmin)
