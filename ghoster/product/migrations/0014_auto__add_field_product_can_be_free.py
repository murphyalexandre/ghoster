# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Product.can_be_free'
        db.add_column(u'product_product', 'can_be_free',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Product.can_be_free'
        db.delete_column(u'product_product', 'can_be_free')


    models = {
        u'artist.artist': {
            'Meta': {'object_name': 'Artist'},
            'courrielleur_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_pwywlist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'courrielleur_subscribelist_id': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True'}),
            'exclusive': ('ckeditor.fields.RichTextField', [], {}),
            'exclusive_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'exclusive_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_pwyw': ('ckeditor.fields.RichTextField', [], {}),
            'faq_pwyw_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_pwyw_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'faq_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'faq_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help': ('ckeditor.fields.RichTextField', [], {}),
            'help_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'help_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'month_contribution': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'stripe_mode': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'stripe_public_live': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_public_test': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_secret_live': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'stripe_secret_test': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'subscribe_txt': ('ckeditor.fields.RichTextField', [], {}),
            'subscribe_txt_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'subscribe_txt_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_and_conditions': ('ckeditor.fields.RichTextField', [], {}),
            'terms_and_conditions_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'terms_and_conditions_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe': ('ckeditor.fields.RichTextField', [], {}),
            'why_subscribe_en': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'}),
            'why_subscribe_fr': ('ckeditor.fields.RichTextField', [], {'null': 'True', 'blank': 'True'})
        },
        u'product.product': {
            'Meta': {'object_name': 'Product'},
            'artist': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'products'", 'to': u"orm['artist.Artist']"}),
            'can_be_free': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'category': ('django.db.models.fields.CharField', [], {'default': "'album'", 'max_length': '30'}),
            'cover': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'null': 'True'}),
            'default_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'min_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '255'}),
            'step_price': ('django.db.models.fields.DecimalField', [], {'max_digits': '5', 'decimal_places': '2'})
        },
        u'product.productformat': {
            'Meta': {'object_name': 'ProductFormat'},
            'description': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'description_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'description_fr': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'help_link': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'help_link_en': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            'help_link_fr': ('django.db.models.fields.CharField', [], {'max_length': '500', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name_fr': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'product': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'formats'", 'to': u"orm['product.Product']"})
        }
    }

    complete_apps = ['product']